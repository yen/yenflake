{ config, pkgs, user, lib, ... }:
let
  # Scripts
  switch-keyboard-layout-script = pkgs.writeShellScriptBin "switch-keyboard-layout" 
  ''
    ### If keyboard map is "us" then set to "dk" else set to us.

    CURRENT_KEYMAP=$(setxkbmap -print | grep symbols | awk '{ print $4 }' | awk -F+ '{ print $2 }')
    [[ $CURRENT_KEYMAP == us ]] && setxkbmap -layout dk || setxkbmap -layout us
  '';

  choose-primary-private-key = pkgs.writeShellScriptBin "SS"
  ''
    KEY_DIR="/home/${user}/.ssh"
    # Function to check if a file is a private SSH key
    is_private_key() {
      local file="$1"
      if grep -q "PRIVATE KEY" "$file" 2>/dev/null; then
        return 0
      else
        return 1
      fi
    }
    # List all private keys in the directory, except id_rsa
    echo "Available SSH keys:"
    i=1
    declare -A keys
    for key in "$KEY_DIR"/*; do
      if [[ -f "$key" && "$(basename "$key")" != "id_rsa" ]]; then
        if is_private_key "$key"; then
          echo "$i) $(basename "$key")"
          keys[$i]="$key"
          ((i++))
        fi
      fi
    done
    # Prompt the user to choose a key by number
    read -p "Enter the number of the private key you want to use: " choice
    # Check if the input is a valid number
    if ! [[ "$choice" =~ ^[0-9]+$ ]]; then
      echo "Invalid input. Please enter a number."
      exit 1
    fi
    # Check if the choice is valid
    if [ -z "''${keys[$choice]}" ]; then
      echo "Invalid choice. Please enter a valid number."
      exit 1
    fi
    # Copy the chosen key to id_rsa in the same directory
    chosen_key="''${keys[$choice]}"
    cp "$chosen_key" "$KEY_DIR/id_rsa"
    echo "The key $(basename "$chosen_key") has been copied to id_rsa."    
  '';
in {

  imports =
    [
      ./hardware-configuration.nix
    ];

  system.activationScripts.ldso = lib.stringAfter [ "usrbinenv" ] ''
    mkdir -m 0755 -p /lib64
    ln -sfn ${pkgs.glibc.out}/lib64/ld-linux-x86-64.so.2 /lib64/ld-linux-x86-64.so.2.tmp
    mv -f /lib64/ld-linux-x86-64.so.2.tmp /lib64/ld-linux-x86-64.so.2 # atomically replace
  '';

  hardware = {
     graphics = {
       enable = true;
       enable32Bit = true;
     };
  };

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      systemd-boot = {
        enable = true;
        configurationLimit = 10;
      };
      efi.canTouchEfiVariables = true;
    };
    tmp.cleanOnBoot = true;
  };

  time.timeZone = "Europe/Copenhagen";
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_TIME = "da_DK.UTF-8";
      LC_MONETARY = "da_DK.UTF-8";
    };
  };

  console = {
    keyMap = "us";
  };

  services.xserver = {
    enable = true;

    xkb.layout = "us,dk";

    desktopManager = {
      xterm.enable = false;
    };

    displayManager = {
      startx.enable = true;
    };

    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [
        dmenu
        i3status-rust
        i3lock
      ];
    };
  };

  specialisation = {
    gaming.configuration = {
      system.nixos.tags = [ "gaming" ];
      services.xserver = {
        displayManager.lightdm.enable = true;
        videoDrivers = [ "nvidia" ];
      };
      hardware.nvidia = {

        modesetting.enable = true;

        powerManagement.enable = false;
        powerManagement.finegrained = false;

        open = true;

        nvidiaSettings = true;

        package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
          version = "570.86.16"; # use new 570 drivers
          sha256_64bit = "sha256-RWPqS7ZUJH9JEAWlfHLGdqrNlavhaR1xMyzs8lJhy9U=";
          openSha256 = "sha256-DuVNA63+pJ8IB7Tw2gM4HbwlOh1bcDg2AN2mbEU9VPE=";
          settingsSha256 = "sha256-9rtqh64TyhDF5fFAYiWl3oDHzKJqyOW3abpcf2iNRT8=";
          usePersistenced = false;
        };

        prime = {
          sync.enable = true;

          intelBusId = "PCI:0:2:0";
          nvidiaBusId = "PCI:1:0:0";
        };
      };
    };
  };
  
  services.libinput = {
    enable = true;
    touchpad.naturalScrolling = true;
    touchpad.scrollMethod = "twofinger";
    touchpad.disableWhileTyping = true;
  };

  security = {
    sudo.wheelNeedsPassword = false;
  };

  nix = {
    package = pkgs.nixVersions.stable;
    extraOptions = "experimental-features = nix-command flakes";
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
  };

  networking = {
    hostName = "yenting";
    extraHosts =
    ''
    127.0.0.1 postgres
    88.99.122.208 dev.hiive.buzz
    88.99.122.208 dev.api.hiive.buzz
    '';
    networkmanager.enable = true;
  };

  fonts.packages = with pkgs; [
    terminus_font
    terminus_font_ttf
    font-awesome
  ];

  users.users.${user} = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" "video" "audio" "uucp" "dialout" "adbusers" "docker" "jackaudio"];
  };

  hardware.bluetooth.enable = true; # enables support for Bluetooth
  hardware.bluetooth.powerOnBoot = true; # powers up the default Bluetooth controller on boot

  hardware.pulseaudio.package = pkgs.pulseaudio.override { jackaudioSupport = true; };
  services.jack = {
    jackd.enable = true;
    # support ALSA only programs via ALSA JACK PCM plugin
    alsa.enable = false;
    # support ALSA only programs via loopback device (supports programs like Steam)
    loopback = {
      enable = true;
      # buffering parameters for dmix device to work with ALSA only semi-professional sound programs
      #dmixConfig = ''
      #  period_size 2048
      #'';
    };
  };

  environment = {
    systemPackages = with pkgs; [
      binutils
      killall
      lshw
      lsof
      pciutils
      usbutils
      htop
      tree
      netcat-gnu
      xclip
      arandr
      sysfsutils
      keepassxc
      ltrace
      file
      moreutils
      dig
      xorg.xev
      xorg.xmodmap
      openssl
      ffmpeg
      findutils
      pinentry
      inetutils
      libserialport

      qemu

      wget
      unzip
      unrar
      picocom
      lf
      jq

      alsa-utils

      # Editors
      tmux
      vim
      helix

      # Development
      haproxy
      docker
      docker-buildx
      platformio
      esptool
      postgresql

      # Python
      python3

      # Rust
      gcc
      rustup
      sqlx-cli
      cargo-tarpaulin
      cargo-watch
      cargo-udeps
      cargo-edit

      # Latex / PDF
      texlive.combined.scheme-full
      zathura

      # Language servers
      nodePackages_latest.bash-language-server
      nil
      clang-tools_16
      python311Packages.python-lsp-server
      yaml-language-server
      nodePackages_latest.typescript-language-server
      lua-language-server
      marksman
      texlab

      # DevOps
      terraform
      terraform-ls
      packer

      # Infosec
      ffuf
      gobuster
      burpsuite
      mitmproxy
      radare2
      nmap

      gnome-network-displays
      scdl

      # Own
      switch-keyboard-layout-script
      choose-primary-private-key

      ranger

      steamcmd
      vibrantlinux
    ];
  };

  xdg.portal.enable = true;
  xdg.portal.config.common.default = "*";
  xdg.portal.xdgOpenUsePortal = true;
  xdg.portal.extraPortals = [
    pkgs.xdg-desktop-portal-gnome
  ];

  virtualisation.docker.enable = true;

  programs = {
    light.enable = true;
    adb.enable = true;
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
    localNetworkGameTransfers.openFirewall = true; # Open ports in the firewall for Steam Local Network Game Transfers
  };

  services.locate = {
    enable = true;
    package = pkgs.mlocate;
    localuser = null;
  };

  services = {
    devmon.enable = true;
    tlp.enable = true;
    auto-cpufreq.enable = true;
    udev.packages = [
      pkgs.android-udev-rules
    ];
  };

  system.stateVersion = "23.05"; # Did you read the comment?
}
