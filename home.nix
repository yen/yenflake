{ pkgs, user, ... }:
{
  home.username = "${user}";
  home.homeDirectory = "/home/${user}";

  home.file = {
    ".xinitrc".source = ./dotfiles/.xinitrc;
    ".Xresources".source = ./dotfiles/.Xresources;
    ".Xdefaults".source = ./dotfiles/.Xdefaults;
    ".config/i3/config".source = pkgs.lib.mkForce ./dotfiles/i3config;
    ".config/i3/i3status_config.toml".source = pkgs.lib.mkForce ./dotfiles/i3status_config.toml;
  };

  programs.bash = {
    enable = true;
    enableCompletion = true;

    bashrcExtra = ''
      PS1="\[\e[38;2;128;0;0m\][\u@\h]:\[\e[38;2;225;225;75m\]\w\$\[\e[0m\] ";

      export SSH_ASKPASS=""
      ulimit -n 52428
      source /home/${user}/.secrets
    '';

    shellAliases = {
      sr = "steam-run";
      ls = "ls --color=auto";
      ll = "ls -alFh";
      la = "ls -A";
      l = "ls -lFh";

      grep = "grep --color=auto";
      fgrep = "fgrep --color=auto";
      egrep = "egrep --color=auto";

      hg = "history | grep";
      xclip = "xclip -selection clipboard";
      latexclean = "rm -r _minted-main *.{aux,log,toc,out,bcf,xml,bbl,blg,bib}";
    };
  };

  programs.helix = {
    enable = true;
    defaultEditor = true;
    settings = {
      theme = "base16_terminal";
      editor = {
        bufferline = "multiple";
        file-picker.hidden = false;
        line-number = "relative";
        rulers = [ 120 ];
        auto-pairs = false;

        cursor-shape = {
          insert = "bar";
          normal = "block";
          select = "underline";
        };

        lsp = {
          display-messages = true;
          display-inlay-hints = false;
        };

        whitespace = {
          render = {
            tab = "all";
          };
          characters = {
            tab = "→";
          };
        };
      };
    };
  };

  programs.gpg = {
    enable = true;
    homedir = "/home/${user}/.gnupg";
  };

  services.gpg-agent = {
    enable = true;
    enableBashIntegration = true;
    pinentryPackage = pkgs.pinentry-curses;
  };

  programs.git = {
    enable = true;
    lfs.enable = true;
    userName = "yen";
    userEmail = "git@yenmail.party";
    aliases = {
      st = "status -s";
      cm = "!git branch -d $(git branch --merged | sed -e s/\\*.*/\\ /g | tr -s \" \" | tr -s \"\\n\" \" \")";
    };
    ignores = [".direnv" ".devenv" ".envrc" ".pio"];
    diff-so-fancy.enable = true;
    includes =
      [
        {
          condition = "gitdir:/home/${user}/projects/**";
          contents = {
            user = {
              email = "yengit@yenmail.party";
              name = "yen";
              signingKey = "120F272B9981E77F";
            };
            commit = {
              gpgSign = true;
            };
          };
        }
      ];
      extraConfig = {
        lfs.cachecredentials = true;
        credential.helper=["cache --timeout=3600"];
      };
  };

  programs.direnv = {
    enable = true;
    enableBashIntegration = true;
    nix-direnv.enable = true;
  };

  programs.thunderbird = {
    enable = true;
    profiles = {
      hiive = {
        isDefault = true;
      };
    };
  };

  nixpkgs.config.allowUnfree = true;

  xsession = {
    enable = true;
    numlock.enable = false;
  };

  home = {
    packages = with pkgs; [
      firefox
      ungoogled-chromium
      pavucontrol
      vlc
      obs-studio
      audacity
      tutanota-desktop
      telegram-desktop
      signal-desktop
      maim
      spotify
      deluge
      guvcview
      s3cmd

      # Images
      feh
      pinta
      imagemagick

      # Video
      libsForQt5.kdenlive
    ];
  };

  home.stateVersion = "23.05";

  programs.home-manager.enable = true;
}
